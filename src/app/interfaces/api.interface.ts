export interface IWordLang {
  name: string;
  definition: string;
}

export interface IWords {
  id: number|string;
  image: string;
  es: IWordLang;
  en: IWordLang;
}
