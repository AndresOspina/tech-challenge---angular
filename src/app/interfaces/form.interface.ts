export interface IWordForm {
  image: string;
  nameEs: string;
  definitionEs: string;
  nameEn: string;
  definitionEn: string;
}