import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import uuid from 'uuid';

import { IWordForm } from './../../interfaces/form.interface';
import { IWords } from './../../interfaces/api.interface';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input() id: number | string = null;

  // Add default values to the input properties
  @Input() image: string = '';
  @Input() nameEs: string = '';
  @Input() definitionEs: string = '';
  @Input() nameEn: string = '';
  @Input() definitionEn: string = '';

  @Output() formHandler = new EventEmitter<IWords>();

  wordForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    // Build the form using the input values as the initial values
    this.wordForm = this.formBuilder.group({
      image: [this.image],
      nameEs: [this.nameEs],
      definitionEs: [this.definitionEs],
      nameEn: [this.nameEn],
      definitionEn: [this.definitionEn]
    });
  }

  formatFormValues(values: IWordForm): IWords {
    return {
      id: this.id !== null ? this.id : uuid(), // If no id, generate a random id
      image: values.image,
      es: {
        name: values.nameEs,
        definition: values.definitionEs
      },
      en: {
        name: values.nameEn,
        definition: values.definitionEn
      }
    };
  }

  onFormSubmit(values: IWordForm) {
    const wordValue: IWords = this.formatFormValues(values);
    this.formHandler.emit(wordValue);
  }
}
